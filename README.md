# md-reader #

This is a markdown reader using [parsedown](https://github.com/erusev/parsedown).

### Feature list ###

* Listing of .md files with alphanumerical names
* Search in filename und content of files using regular expressions
* Parsing files using parsedown
