<?php 
session_start();
include_once('check_login.php');
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" href="css/md.css" />
<style type="text/css">
a,a:active,a:visited { color:#333; }
</style>
<div style="position:fixed;right:8%;top:12px;background:#fff;padding:6px;">
	<a href="listing.php">Listing</a>
	<a href="search.php">Search</a>
</div>
<?php
if(isset($_GET['file'])){
	$file = $_GET['file'];
	if(ctype_alnum($file) && file_exists('files/'.$file.'.md') ){
		include_once 'libs/parsedown.php';
		$md = file_get_contents('files/'.$file.'.md');
		$pd = new Parsedown();
		echo $pd->text( $md );
	}
}