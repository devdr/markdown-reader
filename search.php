<?php 
session_start();
include_once('check_login.php');
?>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" href="css/md.css" />
<style type="text/css">
a,a:active,a:visited { color:#333; }
</style>
</head>
<body>
<div style="position:fixed;right:8%;top:12px;background:#fff;padding:6px;">
	<a href="listing.php">Listing</a>
	<a href="search.php">Search</a>
</div>
<form method="post" action="">
	<input type="text" name="search" value="<?php echo (isset($_POST['search']))?$_POST['search']:''; ?>" placeholder="Search" />
</form>
<?php
if( isset($_POST['search']) ){
	$search = filter_var($_POST['search'],FILTER_SANITIZE_STRING);
	$search = (strpos($search,'/')===0)?$search:'/'.$search.'/i';
	$regexp = $search;
	if ($search !== false && $handle = opendir('files')) {
		echo '<h1>Found in file:</h1>';
		while (false !== ($file = readdir($handle))) {
			if(!(strpos($file,'.') === 0)){
				$expFile = explode('.',$file);
				if( isset($expFile[1]) && $expFile[1] == 'md' ){
					$arrKeys=array();
					$strFile = 'files/' . $expFile[0].'.'.$expFile[1];
					if(file_exists($strFile)){
						$cnt = 0;
						$cnt += preg_match_all($regexp, file_get_contents($strFile), $arrKeys, PREG_PATTERN_ORDER);
						$cnt += preg_match_all($regexp, $expFile[0], $arrKeys, PREG_PATTERN_ORDER);
						if($cnt>0){
							echo "<a href=\"index.php?file=" . $expFile[0] . "\">" . $expFile[0] . " ($cnt)</a><br/>";
						}
					}
					
				}
			}
		}
		closedir($handle);
	}
}
?>
</body>
</html>