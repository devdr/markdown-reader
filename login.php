<?php
session_start();

$booReset = true;
if( isset($_SESSION['usr']) && isset($_SESSION['pwd']) && isset($_POST['sender']) && $_POST['sender'] == 'login'){
	if(isset($_POST[$_SESSION['usr']]) && isset($_POST[$_SESSION['pwd']])){
		$strUsr = filter_var($_POST[$_SESSION['usr']]);
		$strPwd = filter_var($_POST[$_SESSION['pwd']]);
		$strUsers = file_get_contents('files/users');
		$arrUserline = explode("\n",$strUsers);
		foreach($arrUserline as $strLine){
			$arrUsrPwd = explode(":",$strLine);
			if(strlen($arrUsrPwd[0])>0 && $strUsr == $arrUsrPwd[0]){
				if( strlen($arrUsrPwd[1]) > 0 && md5($strPwd) == $arrUsrPwd[1] ){
					$_SESSION['loggedin_md'] = md5($strPwd);
					$booReset = false;
					header('location: index.php');
				}
			}
		}
	}
}

if( isset($_SESSION['loggedin_md']) && $booReset)
	unset($_SESSION['loggedin_md']);

$_SESSION['usr'] = uniqid();
$_SESSION['pwd'] = uniqid();
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" href="css/md.css" />
<style type="text/css">
a,a:active,a:visited { color:#333; }
</style>
<form action="login.php" method="post">
	<input placeholder="benutzer" type="text" name="<?php echo $_SESSION['usr'];?>" value=""><br/>
	<input placeholder="kennwort" type="password" name="<?php echo $_SESSION['pwd'];?>" value=""><br/>
	<input placeholder="pass" type="submit" name="sender" value="login">
</form>